import "./App.css";

import { useEffect, useState } from "react";
import {
    Navigate,
    BrowserRouter as Router,
    Routes,
    Route,
} from "react-router-dom";

import AppNavbar from "./components/AppNavbar.js";
import Footer from "./components/Footer.js";
import NotFound from "./pages/NotFound.js";
import Home from "./pages/Home.js";
import Register from "./pages/Register.js";
import Login from "./pages/Login.js";
import Logout from "./pages/Logout.js";
import Profile from "./pages/Profile.js";
import AddCard from "./pages/AddCard.js";
import AllCardsList from "./pages/AllCardsList.js";
import ActiveCardsList from "./pages/ActiveCardsList.js";
import Cart from "./pages/Cart";
import CheckOut from "./pages/CheckOut";
import OrderHistory from "./pages/OrderHistory";

import { UserProvider } from "./context/UserContext.js";

function App() {
    const [loading, setLoading] = useState(true);
    const [user, setUser] = useState({
        id: null,
        username: null,
        isAdmin: false,
    });

    useEffect(() => {
        if (localStorage.getItem("token")) {
            fetch(`${process.env.REACT_APP_URI}/account/profile`, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
            })
                .then((response) => response.json())
                .then((data) => {
                    setUser({
                        id: data._id,
                        username: data.username,
                        isAdmin: data.isAdmin,
                    });
                    setLoading(false);
                });
        } else {
            setLoading(false);
        }
    }, []);

    const clearUser = () => {
        localStorage.clear();
        setUser({ id: null, username: null, isAdmin: false });
    };

    return (
        <UserProvider value={{ user, setUser, clearUser }}>
            <Router>
                <div className="page-bg">
                    {!loading && <AppNavbar />}

                    <Routes>
                        <Route path="*" element={<NotFound />} />
                        <Route path="/" element={<Home />} />
                        <Route path="/home" element={<Home />} />
                        <Route
                            path="/account/register"
                            element={
                                user.id ? <Navigate to="/home" /> : <Register />
                            }
                        />
                        <Route
                            path="/account/login"
                            element={
                                user.id ? <Navigate to="/home" /> : <Login />
                            }
                        />

                        <Route path="/account/logout" element={<Logout />} />

                        <Route
                            path="/account/profile"
                            element={
                                !user.id ? <Navigate to="/home" /> : <Profile />
                            }
                        />
                        <Route
                            path="/products/cards/list"
                            element={<AllCardsList />}
                        />
                        <Route
                            path="/products/cards/add"
                            element={
                                !user.id && !user.isAdmin ? (
                                    <Navigate to="/home" />
                                ) : (
                                    <AddCard />
                                )
                            }
                        />

                        <Route
                            path="/products/cards/list/active"
                            element={<ActiveCardsList />}
                        />

                        <Route
                            path="/cart/get"
                            element={
                                !user.id && !user.isAdmin ? (
                                    <Navigate to="/home" />
                                ) : (
                                    <Cart />
                                )
                            }
                        />

                        <Route
                            path="/order/checkout"
                            element={
                                !user.id && !user.isAdmin ? (
                                    <Navigate to="/home" />
                                ) : (
                                    <CheckOut />
                                )
                            }
                        />

                        <Route
                            path="/order/list"
                            element={
                                !user.id && !user.isAdmin ? (
                                    <Navigate to="/home" />
                                ) : (
                                    <OrderHistory />
                                )
                            }
                        />
                    </Routes>

                    <Footer />
                </div>
            </Router>
        </UserProvider>
    );
}

export default App;
