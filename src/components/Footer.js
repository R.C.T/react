export default function Footer() {
    return (
        <div className="w-100 bg-dark text-white text-center py-3 mt-5 rounded-top">
            <p className="m-0">
                This is only for a personal project. All credits go to its
                rightful owner.
            </p>
        </div>
    );
}
