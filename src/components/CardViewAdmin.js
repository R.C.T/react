import { useContext, useState, useEffect } from "react";
import {
    Modal,
    Form,
    Card,
    Button,
    Container,
    Row,
    Col,
} from "react-bootstrap";
import CardContext from "../context/CardContext.js";

export default function CardViewAdmin(props) {
    const [showEdit, setShowEdit] = useState(false);
    const [cardStatus, setCardStatus] = useState("");
    const {
        cardId,
        cardName,
        setCardName,
        description,
        setDescription,
        gameName,
        setGameName,
        price,
        setPrice,
        stock,
        setStock,
        isActive,
        setIsActive,
        imgSrc,
        setImgSrc,
    } = useContext(CardContext);

    useEffect(() => {
        isActive ? setCardStatus("Active") : setCardStatus("Archived");
    }, [isActive]);

    function updateCard(event) {
        event.preventDefault();

        fetch(`${process.env.REACT_APP_URI}/products/cards/update`, {
            method: `PATCH`,
            headers: {
                "Content-Type": `application/json`,
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
            body: JSON.stringify({
                cardId: cardId,
                cardName: cardName,
                description: description,
                gameName: gameName,
                price: price,
                stock: stock,
                isActive: isActive,
                imgSrc: imgSrc,
            }),
        })
            .then((response) => response.json())
            .then((data) => {
                if (data.isCardUpdated) {
                    alert(`Card "${cardName}" has been updated successfully.`);
                    setShowEdit(false);
                } else {
                    alert(
                        `There was an error during the process. Please try again.`
                    );
                }
            });
    }

    return (
        <Modal
            {...props}
            size="lg"
            aria-label="cardViewModal"
            onExit={() => {
                setShowEdit(false);
            }}
            centered
            backdrop="static"
            className="fs-5"
        >
            <Modal.Header closeButton className="text-white bg-primary">
                <Modal.Title className="fw-bold pe-2">{`${cardName}`}</Modal.Title>
                <Modal.Title>{`(${cardStatus})`}</Modal.Title>
            </Modal.Header>
            <Modal.Body className="text-white bg-dark">
                {!showEdit ? (
                    <Card.Body className="container p-3">
                        <Row>
                            <Col className="col-5">
                                <img src={imgSrc} className="img-fluid" />
                            </Col>
                            <Col className="col-7">
                                <Card.Title className="fw-bold">
                                    Description:
                                </Card.Title>
                                <Card.Text>{description}</Card.Text>

                                <Card.Title className="fw-bold">
                                    Game:
                                </Card.Title>
                                <Card.Text>{gameName}</Card.Text>

                                <Card.Title className="fw-bold">
                                    Price:
                                </Card.Title>
                                <Card.Text>Php {price}</Card.Text>

                                <Card.Title className="fw-bold">
                                    Stocks:
                                </Card.Title>
                                <Card.Text>{stock}</Card.Text>
                            </Col>
                        </Row>
                    </Card.Body>
                ) : (
                    <Form className="p-3" onSubmit={updateCard}>
                        <Form.Group className="mb-3" controlId="cardName">
                            <Form.Label>Card Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter the card name"
                                value={cardName}
                                onChange={(event) => {
                                    setCardName(event.target.value);
                                }}
                                autoFocus={showEdit}
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="description">
                            <Form.Label>Description</Form.Label>
                            <Form.Control
                                type="text"
                                as="textarea"
                                placeholder="Enter the description"
                                value={description}
                                onChange={(event) => {
                                    setDescription(event.target.value);
                                }}
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="gameName">
                            <Form.Label>Game:</Form.Label>
                            <Form.Select
                                value={gameName}
                                onChange={(event) => {
                                    setGameName(event.target.value);
                                }}
                                required
                            >
                                <option disabled>Select game</option>
                                <option value="Yu-Gi-Oh!">Yu-Gi-Oh!</option>
                                <option value="Magic - The Gathering">
                                    Magic - The Gathering
                                </option>
                            </Form.Select>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="price">
                            <Form.Label>Price:</Form.Label>
                            <Form.Control
                                type="number"
                                placeholder="Enter the price"
                                value={price}
                                onChange={(event) => {
                                    setPrice(event.target.value);
                                }}
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="stock">
                            <Form.Label>Stock:</Form.Label>
                            <Form.Control
                                type="number"
                                placeholder="Enter the stock"
                                value={stock}
                                onChange={(event) => {
                                    setStock(event.target.value);
                                }}
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="imgSrc">
                            <Form.Label>Image URL:</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter the image URL"
                                value={imgSrc}
                                onChange={(event) => {
                                    setImgSrc(event.target.value);
                                }}
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="isActive">
                            <Form.Label>Is Active?</Form.Label>{" "}
                            <Form.Check
                                inline
                                type="switch"
                                aria-label="isActive"
                                checked={isActive}
                                onChange={() => {
                                    setIsActive((prev) => !prev);
                                }}
                            ></Form.Check>
                        </Form.Group>
                    </Form>
                )}
            </Modal.Body>
            <Modal.Footer className="text-white bg-dark">
                {!showEdit ? (
                    <Button variant="primary" onClick={() => setShowEdit(true)}>
                        Edit
                    </Button>
                ) : (
                    <>
                        <Button variant="primary" onClick={updateCard}>
                            Update
                        </Button>
                        <Button
                            variant="secondary"
                            onClick={() => setShowEdit(false)}
                        >
                            Cancel
                        </Button>
                    </>
                )}
            </Modal.Footer>
        </Modal>
    );
}
