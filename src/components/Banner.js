import { useState } from "react";
import { Carousel, Row, Col, Button } from "react-bootstrap";
import { useSwipeable } from "react-swipeable";
import bannerData from "../data/bannerData.js";

export default function Banner() {
    const [index, setIndex] = useState(0);

    const handleSelect = (selectedIndex, e) => {
        setIndex(selectedIndex);
    };

    useSwipeable({
        onSwipedLeft: () => setIndex(index + 1),
        onSwipedRight: () => setIndex(index - 1),
    });

    const imageClick = () => {
        console.log("Banner Click");
    };

    return (
        <Carousel
            activeIndex={index}
            onSelect={handleSelect}
            controls={window.innerWidth <= 576 ? false : true}
            className="w-100"
        >
            {bannerData.map((items) => (
                <Carousel.Item key={items.id}>
                    <img
                        className="carousel-img"
                        src={items.imgSrc}
                        alt={items.alt}
                        onClick={() => imageClick()}
                    />
                </Carousel.Item>
            ))}
        </Carousel>
    );
}
