import { useContext, useState } from "react";
import { Card, Nav } from "react-bootstrap";
import UserContext from "../context/UserContext.js";
import AllCardsList from "../pages/AllCardsList.js";
import AddCard from "../pages/AddCard.js";
import OrderHistory from "../pages/OrderHistory.js";

export default function Dashboard() {
    const { user } = useContext(UserContext);
    const [showList, setshowList] = useState(true);
    const [showAdd, setShowAdd] = useState(false);
    const [showHistory, setShowHistory] = useState(true);

    const showCardsList = () => {
        setshowList(true);
        setShowAdd(false);
    };

    const showAddCard = () => {
        setShowAdd(true);
        setshowList(false);
    };

    return (
        <div>
            {user.id && user.isAdmin ? (
                <div className="">
                    <Nav variant="tabs" defaultActiveKey="viewAllCards">
                        <Nav.Item className="bg-white rounded-top">
                            <Nav.Link
                                active={showList}
                                eventKey="viewAllCards"
                                className="dashboard-navlink"
                                onClick={() => showCardsList()}
                            >
                                View All Cards
                            </Nav.Link>
                        </Nav.Item>
                        <Nav.Item className="bg-white rounded-top">
                            <Nav.Link
                                active={showAdd}
                                eventKey="addCards"
                                className="dashboard-navlink"
                                onClick={() => showAddCard()}
                            >
                                Add Card
                            </Nav.Link>
                        </Nav.Item>
                    </Nav>

                    <div className="bg-dark rounded">
                        {showList && <AllCardsList />}
                        {showAdd && <AddCard />}
                    </div>
                </div>
            ) : (
                <Card>
                    <Card.Header className="bg-secondary">
                        <Nav variant="tabs" defaultActiveKey="orderHistory">
                            <Nav.Item className="bg-light">
                                <Nav.Link
                                    eventKey="orderHistory"
                                    className="dashboard-navlink"
                                    onClick={() => showCardsList()}
                                >
                                    Order History
                                </Nav.Link>
                            </Nav.Item>
                        </Nav>
                    </Card.Header>
                    <Card.Body>{showHistory && <OrderHistory />}</Card.Body>
                </Card>
            )}
        </div>
    );
}
