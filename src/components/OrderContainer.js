import { Fragment, useEffect, useState, useContext } from "react";
import { Row, Col, Card } from "react-bootstrap";

export default function OrderContainer(props) {
    const [productName, setProductName] = useState("");
    const [productPrice, setProductPrice] = useState(0);
    const [productQuantity, setProductQuantity] = useState(0);
    const [purchasedDate, setPurchasedDate] = useState(null);

    useEffect(() => {
        console.log(props);
        setProductName(props.orderProps.productName);
        setProductPrice(props.orderProps.productPrice);
        setProductQuantity(props.orderProps.productQuantity);
        if (props.dateProp) {
            setPurchasedDate(props.dateProp);
        }
    }, []);

    return (
        <Card className="card-container p-3 col-12">
            <Card.Body className="p-0">
                <Row>
                    <Col>
                        <Card.Title className="fw-bold">
                            {productName}
                        </Card.Title>

                        <Card.Subtitle className="fw-bold">
                            Price:
                        </Card.Subtitle>
                        <Card.Text>Php {productPrice}</Card.Text>

                        <Card.Subtitle className="fw-bold">
                            Quantity:
                        </Card.Subtitle>
                        <Card.Text>{productQuantity}</Card.Text>

                        {purchasedDate && (
                            <Fragment>
                                <Card.Subtitle className="fw-bold">
                                    Purchased Date:
                                </Card.Subtitle>
                                <Card.Text>{`${purchasedDate}`}</Card.Text>
                            </Fragment>
                        )}
                    </Col>
                </Row>
            </Card.Body>
        </Card>
    );
}
