import { useContext } from "react";
import { Modal, Card, Button, Row, Col } from "react-bootstrap";
import UserContext from "../context/UserContext.js";
import CardContext from "../context/CardContext.js";

export default function CardViewUser(props) {
    const { user } = useContext(UserContext);

    const { cardId, cardName, description, gameName, price, stock, imgSrc } =
        useContext(CardContext);

    function addToCart() {
        if (user.id) {
            let quantity = prompt(`How many?`);

            if (quantity) {
                fetch(`${process.env.REACT_APP_URI}/cart/add`, {
                    method: `POST`,
                    headers: {
                        "Content-Type": `application/json`,
                        Authorization: `Bearer ${localStorage.getItem(
                            "token"
                        )}`,
                    },
                    body: JSON.stringify({
                        cardId: cardId,
                        quantity: quantity,
                    }),
                })
                    .then((response) => response.json())
                    .then((data) => {
                        if (data) {
                            alert(
                                `Card "${cardName}" has been added to cart successfully `
                            );
                        } else {
                            alert(
                                `There was an error during the process. Please try again.`
                            );
                        }
                    });
            }
        } else {
            alert("Login first");
        }
    }

    return (
        <Modal
            {...props}
            size="lg"
            aria-label="cardViewModal"
            centered
            backdrop="static"
            className="fs-5"
        >
            <Modal.Header closeButton className="text-white bg-primary">
                <Modal.Title className="fw-bold">{`${cardName}`}</Modal.Title>
            </Modal.Header>
            <Modal.Body className="text-white bg-dark">
                <Card.Body className="container p-3">
                    <Row>
                        <Col className="col-5">
                            <img src={imgSrc} className="img-fluid" />
                        </Col>
                        <Col className="col-7">
                            <Card.Title className="fw-bold">
                                Description:
                            </Card.Title>
                            <Card.Text>{description}</Card.Text>

                            <Card.Title className="fw-bold">Game:</Card.Title>
                            <Card.Text>{gameName}</Card.Text>

                            <Card.Title className="fw-bold">Price:</Card.Title>
                            <Card.Text>Php {price}</Card.Text>

                            <Card.Title className="fw-bold">Stocks:</Card.Title>
                            <Card.Text>{stock}</Card.Text>
                        </Col>
                    </Row>
                </Card.Body>
            </Modal.Body>
            <Modal.Footer className="text-white bg-dark">
                <Button variant="primary" onClick={() => addToCart()}>
                    Add to Cart
                </Button>
            </Modal.Footer>
        </Modal>
    );
}
