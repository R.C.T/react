import { useEffect, useState, useContext } from "react";
import CardViewAdmin from "./CardViewAdmin.js";
import CardViewUser from "./CardViewUser.js";
import UserContext from "../context/UserContext.js";
import { CardProvider } from "../context/CardContext.js";

export default function CardImageContainer(props) {
    const { user } = useContext(UserContext);
    const [modalShow, setModalShow] = useState(false);

    /* Card useState */
    const [cardId, setCardId] = useState("");
    const [cardName, setCardName] = useState("");
    const [description, setDescription] = useState("");
    const [gameName, setGameName] = useState("");
    const [price, setPrice] = useState("");
    const [stock, setStock] = useState(0);
    const [isActive, setIsActive] = useState(true);
    const [imgSrc, setImgSrc] = useState("");

    const openCardView = () => {
        setModalShow(true);
    };

    useEffect(() => {
        setCardId(props.cardProp._id);
        setCardName(props.cardProp.cardName);
        setDescription(props.cardProp.description);
        setGameName(props.cardProp.gameName);
        setPrice(props.cardProp.price);
        setStock(props.cardProp.stock);
        setIsActive(props.cardProp.isActive);
        setImgSrc(props.cardProp.imgSrc);
    }, []);

    return (
        <CardProvider
            value={{
                cardId,
                cardName,
                setCardName,
                description,
                setDescription,
                gameName,
                setGameName,
                price,
                setPrice,
                stock,
                setStock,
                isActive,
                setIsActive,
                imgSrc,
                setImgSrc,
            }}
        >
            <div className="card-img-container">
                <img
                    src={imgSrc}
                    className="card-img"
                    onClick={() => openCardView()}
                />
                <div className="card-img-text-container">
                    <div
                        className="card-img-text"
                        onClick={() => openCardView()}
                    >
                        View Details
                    </div>
                </div>
            </div>

            {user.isAdmin ? (
                <CardViewAdmin
                    cardprop={props.cardProp}
                    show={modalShow}
                    onHide={() => {
                        setModalShow(false);
                    }}
                />
            ) : (
                <CardViewUser
                    cardprop={props.cardProp}
                    show={modalShow}
                    onHide={() => {
                        setModalShow(false);
                    }}
                />
            )}
        </CardProvider>
    );
}
