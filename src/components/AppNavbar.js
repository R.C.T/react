import { Fragment, useContext } from "react";
import {
    Container,
    Row,
    Col,
    Nav,
    Navbar,
    Form,
    Button,
} from "react-bootstrap";
import { NavLink } from "react-router-dom";
import UserContext from "../context/UserContext.js";

export default function AppNavbar() {
    const { user } = useContext(UserContext);

    return (
        <Navbar
            variant="dark"
            bg="dark"
            expand="sm"
            className="w-100 rounded-bottom sticky-top fs-5 px-5"
        >
            <Navbar.Brand as={NavLink} to="/home" className="fw-bold fs-1">
                TCG
            </Navbar.Brand>

            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                {/* Search Bar */}
                {/* {!user.isAdmin && ( */}
                {/* <Form className="d-flex">
                            <Form.Control
                                type="search"
                                placeholder="Search"
                                className="me-2"
                                aria-label="Search"
                            />
                            <Button variant="outline-light">Search</Button>
                        </Form> */}
                {/* )} */}

                <Nav variant="pills" className="ms-auto">
                    {user.username && (
                        <div className="text-white d-flex align-items-center pe-2">{` Welcome [ ${user.username} ]`}</div>
                    )}

                    {/* Home */}
                    {/* <Nav.Link as={NavLink} to="/home">
                        Home
                    </Nav.Link> */}

                    {/* Active Cards */}
                    <Nav.Link as={NavLink} to="/products/cards/list/active">
                        Cards
                    </Nav.Link>

                    {!user.id ? (
                        <Fragment>
                            {/* Login */}
                            <Nav.Link as={NavLink} to="/account/login">
                                Login
                            </Nav.Link>

                            {/* Register */}
                            <Nav.Link as={NavLink} to="/account/register">
                                Register
                            </Nav.Link>
                        </Fragment>
                    ) : (
                        <Fragment>
                            {/* Admin Dashboard/User Profile */}
                            {user.isAdmin ? (
                                <Nav.Link as={NavLink} to="/account/profile">
                                    Dashboard
                                </Nav.Link>
                            ) : (
                                <Fragment>
                                    <Nav.Link
                                        as={NavLink}
                                        to="/account/profile"
                                    >
                                        Account
                                    </Nav.Link>
                                    <Nav.Link as={NavLink} to="/cart/get">
                                        Cart
                                    </Nav.Link>
                                </Fragment>
                            )}

                            {/* Logout */}
                            <Nav.Link as={NavLink} to="/account/logout">
                                Logout
                            </Nav.Link>
                        </Fragment>
                    )}
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}
