import { useContext } from "react";
import { Card, Row, Col } from "react-bootstrap";
import UserContext from "../context/UserContext.js";

export default function CardContainer(props) {
    const { user } = useContext(UserContext);

    return (
        <Card className="container p-3 bg-transparent border-0 ">
            <Row>
                <Col>
                    <Card.Title className="fw-bold">
                        {props.cardProp.cardName}
                    </Card.Title>
                </Col>
            </Row>
            <Row className="justify-content-between">
                <Col>
                    <Card.Subtitle className="fw-bold">
                        Description:
                    </Card.Subtitle>
                    <Card.Text>{props.cardProp.description}</Card.Text>

                    <Card.Subtitle className="fw-bold">Game:</Card.Subtitle>
                    <Card.Text>{props.cardProp.gameName}</Card.Text>

                    {user.isAdmin && (
                        <>
                            <Card.Subtitle className="fw-bold">
                                Is Active?
                            </Card.Subtitle>
                            <Card.Text>
                                {props.cardProp.isActive ? ` Yes` : ` No`}
                            </Card.Text>
                        </>
                    )}
                </Col>
                <Col className="col-2">
                    <Card.Subtitle className="fw-bold">Stocks:</Card.Subtitle>
                    <Card.Text>{props.cardProp.stock}</Card.Text>
                </Col>
                <Col className="col-2">
                    <Card.Subtitle className="fw-bold">Price:</Card.Subtitle>
                    <Card.Text>Php {props.cardProp.price}</Card.Text>
                </Col>
            </Row>
        </Card>
    );
}
