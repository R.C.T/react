import { Container, Row, Col } from "react-bootstrap";

export default function Highlights() {
    return (
        <Container className="w-75 p-5 bg-transparent text-white">
            <Row>
                <Col>
                    <h3>Featured Cards</h3>
                </Col>
            </Row>
            <Row className="text-center">
                <Col>
                    <img
                        src="https://drive.google.com/uc?export=view&id=1I99guVvXE8bX0I44oINtz-2iO_M31NQQ"
                        alt="card-back"
                        width="180"
                    />
                </Col>
                <Col>
                    <img
                        src="https://drive.google.com/uc?export=view&id=1I99guVvXE8bX0I44oINtz-2iO_M31NQQ"
                        alt="card-back"
                        width="180"
                    />
                </Col>
                <Col>
                    <img
                        src="https://drive.google.com/uc?export=view&id=1I99guVvXE8bX0I44oINtz-2iO_M31NQQ"
                        alt="card-back"
                        width="180"
                    />
                </Col>
                <Col>
                    <img
                        src="https://drive.google.com/uc?export=view&id=1I99guVvXE8bX0I44oINtz-2iO_M31NQQ"
                        alt="card-back"
                        width="180"
                    />
                </Col>
                <Col>
                    <img
                        src="https://drive.google.com/uc?export=view&id=1I99guVvXE8bX0I44oINtz-2iO_M31NQQ"
                        alt="card-back"
                        width="180"
                    />
                </Col> 
            </Row>
        </Container>
    );
}
