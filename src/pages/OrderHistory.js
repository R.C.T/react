import { useEffect, useState } from "react";
import OrderContainer from "../components/OrderContainer";

export default function OrderHistory() {
    const [orderList, setOrderList] = useState([]);

    const [order, setOrder] = useState({
        id: null,
        userId: null,
        products: [],
        totalAmount: 0,
        dateAdded: null,
    });

    const [product, setProduct] = useState([]);

    useEffect(() => {
        // console.log(orderList);
        orderList.map((data) => {
            setOrder({
                id: data._id,
                products: data.products,
                totalAmount: data.totalAmount,
                purchasedOn: data.purchasedOn,
            });

        });
    }, [orderList]);

    useEffect(() => {
        // console.log("test");
        setProduct(
            order.products.map((item) => {
                return (
                    <OrderContainer
                        orderProps={item}
                        dateProp={order.purchasedOn}
                        key={item._id}
                    />
                );
            })
        );
    }, [order]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_URI}/order/list`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
        })
            .then((response) => response.json())
            .then((data) => {
                if (data) {
                    setOrderList(data);
                }
            });
    }, []);

    return (
        <div className="m-3 d-flex flex-column">
            <h4>List of orders</h4>
            <div className="d-flex flex-wrap">{product}</div>
        </div>
    );
}
