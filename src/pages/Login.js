import { useEffect, useState, useContext } from "react";
import { Button, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import UserContext from "../context/UserContext.js";

export default function Login() {
    const [email, setEmail] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [isDisabled, setIsDisabled] = useState(true);

    const { setUser } = useContext(UserContext);

    useEffect(() => {
        if ((email && password) || (username && password)) {
            setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }
    }, [email, password]);

    function loginUser(event) {
        event.preventDefault();

        fetch(`${process.env.REACT_APP_URI}/account/login`, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                email: email,
                username: username,
                password: password,
            }),
        })
            .then((response) => response.json())
            .then((data) => {
                if (data.accessToken) {
                    localStorage.setItem("token", data.accessToken);
                    retrieveUserDetails(data.accessToken);
                    alert("Login Successful");
                } else {
                    alert("Authentication Failed");
                    setPassword("");
                }
            });

        const retrieveUserDetails = (token) => {
            fetch(`${process.env.REACT_APP_URI}/account/profile`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            })
                .then((response) => response.json())
                .then((data) => {
                    setUser({
                        id: data._id,
                        username: data.username,
                        isAdmin: data.isAdmin,
                    });
                });
        };
    }

    return (
        <Form
            className="w-25 p-5 rounded text-white bg-dark"
            onSubmit={loginUser}
        >
            <Form.Group className="my-3" controlId="email">
                <Form.Label>Username / Email address</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter username or email"
                    value={email ? email : username}
                    onChange={(event) => {
                        if (event.target.value.includes("@")) {
                            setEmail(event.target.value);
                            setUsername("");
                        } else {
                            setUsername(event.target.value);
                            setEmail("");
                        }
                    }}
                    required
                />
            </Form.Group>

            <Form.Group className="my-3" controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control
                    type="password"
                    placeholder="Enter your password"
                    value={password}
                    onChange={(event) => {
                        setPassword(event.target.value);
                    }}
                    required
                />
            </Form.Group>

            <div className="d-flex flex-column justify-content-center align-items-center">
                <Button
                    variant="primary"
                    type="submit"
                    disabled={isDisabled}
                    className="my-3 w-100"
                >
                    Login
                </Button>

                <div className="d-flex">
                    <p className="m-0 pe-2">Don't have an account yet?</p>
                    <Link to="/account/register">Sign-up here</Link>
                </div>
            </div>
        </Form>
    );
}
