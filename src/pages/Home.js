import Banner from "../components/Banner.js";
import Highlights from "../components/Highlights.js";

export default function Home() {
    return (
        <div className="d-flex flex-column w-100 align-items-center justify-content-between">
            <Banner />
            <Highlights />
            <Highlights />
        </div>
    );
}
