import { useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";

export default function Register() {
    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    const [username, setUsername] = useState("");
    const [isActive, setIsActive] = useState(false);
    const redirect = useNavigate();

    useEffect(() => {
        if (
            email &&
            password1 &&
            password2 &&
            username &&
            password1 === password2
        ) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email, password1, password2, username]);

    function registerUser(event) {
        event.preventDefault();

        fetch(`${process.env.REACT_APP_URI}/account/register`, {
            method: `POST`,
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                username: username,
                email: email,
                password: password1,
            }),
        })
            .then((response) => response.json())
            .then((data) => {
                console.log(data);
                if (data.emailExists) {
                    alert("Email has already registered!");
                    setEmail("");
                } else if (data.usernameExists) {
                    alert("Username has already been used!");
                    setUsername("");
                } else {
                    alert("Registration Successful");
                    clearInputs();
                    redirect("/account/login");
                }
            });
    }

    function clearInputs() {
        setEmail("");
        setPassword1("");
        setPassword2("");
        setUsername("");
    }

    return (
        <Form
            className="w-25 p-5 rounded text-white bg-dark"
            onSubmit={registerUser}
        >
            <Form.Group className="mb-3" controlId="username">
                <Form.Label>Username</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter your desired username"
                    value={username}
                    onChange={(event) => {
                        setUsername(
                            // Disable special characters except "_"
                            event.target.value.replace(/[^\w\s]/gi, "")
                        );
                    }}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="email">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                    type="email"
                    placeholder="Enter your email"
                    value={email}
                    onChange={(event) => {
                        setEmail(event.target.value);
                    }}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control
                    type="password"
                    placeholder="Enter your desired password"
                    value={password1}
                    onChange={(event) => {
                        setPassword1(event.target.value);
                    }}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password2">
                <Form.Label>Repeat Password</Form.Label>
                <Form.Control
                    type="password"
                    placeholder="Repeat your desired password"
                    value={password2}
                    onChange={(event) => {
                        setPassword2(event.target.value);
                    }}
                    required
                />
            </Form.Group>

            <div className="d-flex flex-column justify-content-center align-items-center">
                <Button
                    variant="primary"
                    type="submit"
                    disabled={!isActive}
                    className="my-3 w-100"
                >
                    Register
                </Button>

                <div className="d-flex">
                    <p className="m-0 pe-2">Already have an account?</p>
                    <Link to="/account/login">Log-in here</Link>
                </div>
            </div>
        </Form>
    );
}
