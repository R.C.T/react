import { useState, useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import CardInfoContainer from "../components/CardInfoContainer.js";
import CardImgContainer from "../components/CardImageContainer.js";

export default function ActiveCardsList() {
    const [activeCards, setActiveCards] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_URI}/products/cards/list/active`)
            .then((response) => response.json())
            .then((cardsList) => {
                setActiveCards(
                    cardsList.map((card) => {
                        return (
                            <Row
                                key={card._id}
                                className="text-white align-items-center rounded mb-3"
                            >
                                <Col className="text-center col-3">
                                    <CardImgContainer cardProp={card} />
                                </Col>
                                <Col className="col-9">
                                    <CardInfoContainer cardProp={card} />
                                </Col>
                            </Row>
                        );
                    })
                );
            });
    }, []);

    return (
        <Container>
            <Row className="mt-3 text-center text-white">
                <Col>
                    <h2 className="fs-1 fw-bold m-0">List of cards</h2>
                </Col>
            </Row>
            {activeCards}
        </Container>
    );
}
