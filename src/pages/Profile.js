import { useContext } from "react";
import Dashboard from "../components/Dashboard";
import UserContext from "../context/UserContext.js";

export default function Profile() {
    const { user } = useContext(UserContext);

    return (
        <div className="d-flex flex-column w-75 bg-transparent">
            {user.id && user.isAdmin ? (
                <div className="my-5">
                    <h1 className="text-white text-center">Admin Dashboard</h1>
                    <Dashboard />
                </div>
            ) : (
                <div>
                    <h1 className="text-white text-center">Profile</h1>
                    <Dashboard />
                </div>
            )}
        </div>
    );
}
