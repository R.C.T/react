import { useState, useEffect } from "react";
import { Col, Container, Row } from "react-bootstrap";
import CardInfoContainer from "../components/CardInfoContainer.js";
import CardImgContainer from "../components/CardImageContainer.js";

export default function AllCardsList() {
    const [card, setCard] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_URI}/products/cards/list`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
        })
            .then((response) => response.json())
            .then((cardsList) => {
                setCard(
                    cardsList.map((card) => {
                        return (
                            <Container key={card._id} className="px-5 py-3">
                                <Row className="text-dark align-items-center rounded">
                                    <Col className="text-center col-3 ">
                                        <CardImgContainer cardProp={card} />
                                    </Col>
                                    <Col className="col-9 text-white">
                                        <CardInfoContainer cardProp={card} />
                                    </Col>
                                </Row>
                            </Container>
                        );
                    })
                );
            });
    }, []);

    return (
        <div>
            {/* Search bar */}
            {/* <Form className="d-flex my-3">
                        <Form.Control
                            type="search"
                            placeholder="Search"
                            className="me-2"
                            aria-label="Search"
                        />
                        <Button variant="primary">Search</Button>
                    </Form> */}
            {card}
        </div>
    );
}
